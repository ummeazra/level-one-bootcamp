//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

float input()
{
    float a;
 printf("Enter a number: ");
 scanf("%f",&a);
 return a;
}
float compute(float a, float b)
{
    float c;
    c = a+b;
    return c;
}

float output(float a,float b, float c)
{
    printf("The sum of %f and %f = %f",a,b,c);
}
float main()
{
 float x, y, summ, outp;
 x = input();
 y = input();
 summ = compute(x,y);
 outp = output(x,y,summ);
 return 0;
}