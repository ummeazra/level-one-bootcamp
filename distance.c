//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
double input()
{
double x;
scanf("%lf",&x);
return x;
}
double compute(double x1, double y1, double x2, double y2)
{
double c;
c = sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
return c;
}
double output(double x1, double y1, double x2, double y2, double d)
{
printf("The distance between point %lf, %lf and point %lf, %lf = %lf\n",x1,y1,x2,y2,d);
}
int main()
{
double a1, b1, a2, b2, distance, outp;
printf("Enter the abscissa and ordinate of point 1 and 2, respectively: ");
a1 = input();
b1 = input();
a2 = input();
b2 = input();
distance = compute(a1,b1,a2,b2);
outp = output(a1,b1,a2,b2,distance);
return 0;
}
