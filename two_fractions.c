//WAP to find the sum of two fractions.

#include<stdio.h>

struct fractions
{
    int num,den;
};
typedef struct fractions Frac;

Frac ip(Frac a)
{
    scanf("%d%d",&a.num,&a.den);
    return a;
}

int GCD(int a, int b)
{
if(a==0)
{
return b;
}
return GCD(b%a, a);
}

Frac adding(Frac a, Frac b)
{
Frac c;
int N = (b.den*a.num)+(a.den*b.num);
int D = (a.den*b.den);
int fact = GCD(N,D);
c.num = N/fact;
c.den = D/fact;
return c;
}
void op(Frac a,Frac b)
{
printf("Sum of %d/%d + %d/%d is %d/%d \n", a.num, a.den, b.num, b.den, adding(a, b).num, adding(a, b).den);
}
int main()
{
printf("enter 1st numerator and denominator : ");
Frac a = ip(a);
printf("enter 2nd numerator and denominator : ");
Frac b = ip(b);
op(a,b);
return 0;
}
